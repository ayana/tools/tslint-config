import * as NODEJS_FIRST_PARTY_MODULES from 'builtin-modules';

const AYANA_MODULES = [
	'@ayana\/[a-z-]+',
];

module.exports = {
	extends: 'tslint-config-xo',
	rules: {
		'array-type': [
			true,
			'generic',
		],
		'arrow-return-shorthand': true,
		'await-promise': true,
		'callable-types': true,
		'class-name': true,
		'comment-format': [
			true,
			'check-space',
		],
		'curly': [
			true,
			'ignore-same-line',
		],
		'deprecation': true,
		'encoding': true,
		'file-name-casing': [
			true,
			'pascal-case',
			{
				'index.ts': 'ignored',
			},
		],
		'import-spacing': true,
		'interface-over-type-literal': true,
		'jsdoc-format': true,
		'match-default-export-name': true,
		'member-access': [
			true,
			'check-accessor',
			'check-constructor',
			'check-parameter-property',
		],
		'newline-before-return': true,
		'no-angle-bracket-type-assertion': true,
		// Currently disabled until https://github.com/palantir/tslint/pull/4782 lands in NPM
		// 'no-async-without-await': true,
		'no-boolean-literal-compare': true,
		'no-conditional-assignment': true,
		'no-empty-character-class': true,
		'no-duplicate-imports': true,
		'no-duplicate-super': true,
		'no-dynamic-delete': true,
		'no-floating-promises': true,
		'no-for-in-array': true,
		'no-implicit-dependencies': [
			true,
			'optional',
			[
				'@ayana/test',
			],
		],
		'no-inferred-empty-object-type': true,
		'no-magic-numbers': true,
		'no-null-undefined-union': true,
		'no-redundant-jsdoc': true,
		'no-tautology-expression': true,
		'no-unnecessary-callback-wrapper': true,
		'no-void-expression': [
			true,
			'ignore-arrow-function-shorthand',
		],
		'object-curly-spacing': [
			true,
			'always',
		],
		'object-literal-key-quotes': [
			true,
			'consistent-as-needed',
		],
		'ordered-imports': [
			true,
			{
				'grouped-imports': true,
				'groups': [
					{
						name: '@ayana/test',
						match: `^@ayana\/test.*$`,
						order: 10,
					}, {
						name: 'NodeJS first-party modules',
						match: `^(${NODEJS_FIRST_PARTY_MODULES.join('|')})$`,
						order: 20,
					}, {
						name: 'Ayana modules',
						match: `^${AYANA_MODULES.join('|')}$`,
						order: 40,
					}, {
						name: 'Imports from directories above the current one',
						match: '^..\/',
						order: 50,
					}, {
						name: 'Imports from the current directory or directories below',
						match: '^.\/',
						order: 60,
					}, {
						name: 'Third-party modules',
						match: '.*',
						order: 30,
					},
				],
			},
		],
		'prefer-const': true,
		'prefer-for-of': true,
		'prefer-object-spread': true,
		'prefer-readonly': true,
		'promise-function-async': true,
		'restrict-plus-operands': true,
		'space-before-function-paren': [
			true,
			{
				anonymous: 'never',
				named: 'never',
				asyncArrow: 'always',
			},
		],
		'static-this': true,
		'switch-final-break': [
			true,
			'always',
		],
		'trailing-comma': [
			true,
			{
				multiline: {
					objects: 'always',
					arrays: 'always',
					functions: 'never',
					typeLiterals: 'ignore',
				},
				singleline: 'never',
				esSpecCompliant: true,
			},
		],
		'triple-equals': [
			true,
			'allow-null-check',
		],
		'typedef-whitespace': [
			true,
			{
				'call-signature': 'nospace',
				'index-signature': 'nospace',
				'parameter': 'nospace',
				'property-declaration': 'nospace',
				'variable-declaration': 'nospace',
			},
			{
				'call-signature': 'onespace',
				'index-signature': 'onespace',
				'parameter': 'onespace',
				'property-declaration': 'onespace',
				'variable-declaration': 'onespace',
			},
		],
		'unnecessary-bind': true,
		'use-default-type-parameter': true,
		'variable-name': [
			true,
			'ban-keywords',
			'check-format',
			'allow-leading-underscore',
			'require-const-for-all-caps',
		],
		'whitespace': [
			true,
			'check-branch',
			'check-decl',
			'check-operator',
			'check-module',
			'check-separator',
			'check-rest-spread',
			'check-type',
			'check-typecast',
			'check-type-operator',
			'check-preblock',
		],
	},
};
